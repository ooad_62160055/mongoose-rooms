const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  // const room = await Room.findById('6219f39c5df8b34e8cdb2659')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  // const room = await Room.findOne({ name: '3c01' })
  // console.log(room)

  // const room = await Room.find({ capacity: { $gt: 100 } })
  // console.log(room)

  const rooms = await Room.find({ capacity: { $gt: 100 } }).populate('building')
  console.log(rooms)

  const buildings = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(buildings))
}

main().then(() => {
  console.log('Finish')
})
