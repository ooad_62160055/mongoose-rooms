const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  const newInformaticsBuilding = await Building.findById('6219f39c5df8b34e8cdb2658')
  const room = await Room.findById('6219f39c5df8b34e8cdb265d')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)

  room.save()
  informaticsBuilding.save()
  newInformaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
